<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <title>La passion de construire depuis 1989 | Logisko</title>
    <meta name="description" content="Logisko, c'est une histoire de passion et d'entrepreneuriat dans le domaine de la construction résidentielle, industrielle et commerciale depuis plus de 28 ans." />
    <link href="dist/css/reset.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="dist/css/sliderpro/slider-pro.min.css" media="screen"/>
    <link href="dist/css/styles.min.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>

<?php include('inc/header.inc.php'); ?>


<section id="img-top"></section>



<section id="contenu">
	<div class="inner">
    
            <h1> La passion de construire depuis 1989 </h1>
            
            <p>Logisko, c’est d’abord et avant tout le rêve d’un entrepreneur. Présent sur les chantiers dès l’âge de 16 ans, Éric Duchaine a débuté comme coffreur avant d’obtenir ses cartes d’apprenti menuisier. Curieux de nature et aimant toucher à tout, l’homme d’affaires lance sa première entreprise spécialisée dans le domaine de la construction en 1989.</p>  

            <p>Il opère cette compagnie pendant près de 18 ans et réalise de nombreux projets, dont des résidences de prestige, des complexes de condominiums ainsi que des immeubles commerciaux et industriels. En 2006, il se lance un nouveau défi : le développement de terrains. </p>
            <p>Fidèle à ses premiers amours, il actualise ensuite sa licence aux nouvelles normes d’entrepreneur général et élabore des projets multilogements d’envergure. Toujours à la fine pointe de son industrie, il se spécialise dans la construction Novoclimat 2.0 depuis 2016.
            </p>

<br />
			<h2>28 ans de succès</h2>


			<div class="timeline">
            	<div class="annee-left">1989</div>
            	
                <div class="histoire-right">Début de Les Habitations Domicil inc., constructeur de maisons unifamiliales moyennes et haut de gamme.</div>
            </div>

			<div class="timeline">
                  <div class="histoire-left">Début du projet de construction de maison dans Valmont-sur-Parcs à Repentigny.</div>
          		  <div class="annee-right">1991</div>
            	
            </div>
            
            
            
			<div class="timeline">
            	<div class="annee-left">1991</div>
                <div class="histoire-right">Les Habitations Domicil inc. se hisse au deuxième rang des constructeurs, sur 44, pour le nombre d’unités de maisons vendues à Repentigny.</div>
            </div>


			<div class="timeline">
                  <div class="histoire-left">Les Habitations Domicil inc. reçoit le Trophée Méritas décerné par les Chambres de Commerce de Lanaudière Sud dans la catégorie Jeune Entreprise de l’année.</div>
          		  <div class="annee-right">1992</div>
            	
            </div>
            
            
            
            
			<div class="timeline">
            	<div class="annee-left">1993</div>
                <div class="histoire-right">Début de Les Constructions Domicil inc., division de Les Habitations Domicil inc., pour la construction de condominiums.</div>
            </div>


			<div class="timeline">
                  <div class="histoire-left">Lancement par Les Constructions Domicil inc. du projet de condominiums Le Havre du Saint-Laurent, comptant 61 unités dont 21 avec garages intérieurs, situé en bordure du fleuve St-Laurent à Repentigny.</div>
          		  <div class="annee-right">1993</div>
            	
            </div>
            
            
			<div class="timeline">
            	<div class="annee-left">1994</div>
                <div class="histoire-right">Début de Corporation Immobilière Tenor inc., division de Les Habitations Domicil inc., pour l’achat et la vente de terres en vrac et de terrains lotis.</div>
            </div>


			<div class="timeline">
                  <div class="histoire-left">Les Habitations Domicil inc. se fait désigner le titre du plus important constructeur de la région de Lanaudière par le Journal de Montréal.</div>
          		  <div class="annee-right">1996</div>
            	
            </div>
            
            
			<div class="timeline">
            	<div class="annee-left">1997</div>
                <div class="histoire-right">Lancement par Les Constructions Domicil inc. du projet de condominiums Le Sieur du Saint-Laurent, comptant 24 unités situées en bordure du fleuve Saint-Laurent à Repentigny. </div>
            </div>


			<div class="timeline">
                  <div class="histoire-left">Création de la division immobilière avec l’achat de multilogements ainsi que d’édices commerciaux.</div>
          		  <div class="annee-right">1997</div>
            	
            </div>
            
            
			<div class="timeline">
            	<div class="annee-left">1997</div>
                <div class="histoire-right">Rénovation complète d’un immeuble de 28 logements , 197-198 Notre-Dame à Repentigny.</div>
            </div>


			<div class="timeline">
                  <div class="histoire-left">Lancement par Les Constructions Domicil inc. du projet de condominiums Le Versaille, comptant 17 unités situées au centre-ville de Repentigny. </div>
          		  <div class="annee-right">1998</div>
            	
            </div>
            
            
			<div class="timeline">
            	<div class="annee-left">1998</div>
                <div class="histoire-right">Corporation Immobilière Tenor inc. acquiert le projet domiciliaire Le Domaine de la Pinière à Terrebonne.</div>
            </div>


			<div class="timeline">
                  <div class="histoire-left">Les Constructions Domicil inc. remporte le trophée Émérite décerné par Qualité Habitation pour le Projet multifamilial de l’année.</div>
          		  <div class="annee-right">1999</div>
            	
            </div>
            
            
			<div class="timeline">
            	<div class="annee-left">1999</div>
                <div class="histoire-right">Construction du 300 Valmont à Repentigny.</div>
            </div>


			<div class="timeline">
                  <div class="histoire-left">Corporation Immobilière Tenor inc. remporte le prix Domus décerné par l’A.P.C.H.Q. pour le Projet résidentiel de l’année.</div>
          		  <div class="annee-right">2000</div>
            	
            </div>
            
            
			<div class="timeline">
            	<div class="annee-left">2001</div>
                <div class="histoire-right">Corporation Immobilière Tenor inc. est en nomination pour Le projet au gaz de l’année décerné par Gaz Métro.</div>
            </div>


			<div class="timeline">
                  <div class="histoire-left">Dernière maison unifamiliale du projet Valmont-sur-Parc, ce qui fait un total de 180 maisons familiales faisant partie de ce projet. </div>
          		  <div class="annee-right">2002</div>
            	
            </div>
            
            
			<div class="timeline">
            	<div class="annee-left">2003</div>
                <div class="histoire-right">Lancement par Corporation Immobilière Tenor inc. du projet domiciliaire Le Boisé de la Pinière à Terrebonne.</div>
            </div>


			<div class="timeline">
                  <div class="histoire-left">Corporation Immobilière Tenor inc. acquiert plusieurs terres à Terrebonne pour du développement résidentiel et commercial.</div>
          		  <div class="annee-right">2004</div>
            	
            </div>
            
            
			<div class="timeline">
            	<div class="annee-left">2004</div>
                <div class="histoire-right">Construction d’un édifice commercial à Lachenaie, le 950 des Pionniers, Terrebonne.</div>
            </div>


			<div class="timeline">
                  <div class="histoire-left">Construction de la plus prestigieuse résidence dans le Boisé de la Pinière à Terrebonne.</div>
          		  <div class="annee-right">2005</div>
            	
            </div>
            
            
			<div class="timeline">
            	<div class="annee-left">2005</div>
                <div class="histoire-right">Les Habitations Domicil inc. remporte le trophée Émérite décerné par Qualité Habitation pour la Maison Unifamiliale de prestige de l’année.</div>
            </div>


			<div class="timeline">
                  <div class="histoire-left">Construction de l’usine située au 2311 Boulevard des Entreprises à Terrebonne.</div>
          		  <div class="annee-right">2005</div>
            	
            </div>
            
            
			<div class="timeline">
            	<div class="annee-left">2006</div>
                <div class="histoire-right">Fin des opérations pour Les Habitations Domicil inc. et Les Constructions Domicil inc. pour se concentrer sur les activités de Corporation Immobilière Tenor inc., soit l’achat et la vente de terres en vrac et de terrains lotis.</div>
            </div>

			<div class="timeline">
                  <div class="histoire-left">Renouvellement de la licence d’entrepreneur général, avec une mise à jour du code du bâtiment. Début des opérations de l’entreprise Logisko (7107391 Canada Inc).</div>
          		  <div class="annee-right">2009</div>
            	
            </div>
            
            
			<div class="timeline">
            	<div class="annee-left">2012</div>
                <div class="histoire-right">Construction tant résidentielle que commerciale.</div>
            </div>


			<div class="timeline">
                  <div class="histoire-left">Accréditation Novoclimat 2.0</div>
          		  <div class="annee-right">2016</div>
            	
            </div>
            
    </div>
    <!-- inner -->
</section>
<!-- contenu -->


<?php include('inc/footer.inc.php'); ?>

</body>
</html>
