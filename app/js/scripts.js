// JavaScript Document

$(document).ready(function(){
	"use strict";
	
	
	
		$( '#carrousel-accueil' ).sliderPro({
			width: 988,
			height: 440,
			fade: true,
			arrows: true,
			buttons: false,
			smallSize: 300,
			autoplay: true
		});


		$('.projets li').mouseenter(function(){
			$(this).find('div.desc').fadeIn();
			
		}).mouseleave(function(){
			$(this).find('div.desc').hide();
		});

		

		$('#constructeur').waypoint(function() {
		   $('.pop').addClass('add-bounce');
		}, {
		  offset: '600'
		});
		


	
	
$(window).on('scroll', function () {
    var scrollTop = $(window).scrollTop();
    if ( scrollTop > 50 ) {
			
			if( window.innerWidth >= 768 ){
				$('#header').addClass('sticky');
				$('#logo').addClass('logo-sticky');
				$('section#header .inner nav ul#menu').addClass('sticky-menu');
			}

		
	}else{
		
			if( window.innerWidth >= 768 ){
				$('#logo').removeClass('logo-sticky');
				$('#header').removeClass('sticky');
				$('section#header .inner nav ul#menu').removeClass('sticky-menu');
			}

	}
	
});

	
	
	
	
	
	
	$('#menu-icon').on('click', function(){
		
		//$('ul.menu').toggle();
		$('#menu').slideToggle( "slow", function() {
			// Animation complete.

			if ($(this).is(':hidden')) {
				$('#menu-icon').html('&#9776;');
			} else {
				$('#menu-icon').html('&Chi;');
			}
			
		});
		
	});
	

					
					
						
	
 		$("#form-contact").bind("submit", function() {
			
			var erreur = '';
			
			$('#prenom, #nom, #courriel, #telephone, #message').removeClass('erreur');
			/*
			if ( $("#nom").val().length < 1 || $("#titre").val().length < 1 || $("#courriel").val().length < 1 || $("#telephone").val().length < 1 || $("#compagnie").val().length < 1 ) {
				$('#msg').html("<div class='erreur-msg'>Veuillez remplir tous les champs.</div>");
				$('#nom, #titre, #courriel, #telephone, #compagnie').addClass('erreur');
				return false;
			}*/


			if ( $("#prenom").val().length < 1  ) {
				erreur = 1;
				$('#msg').html("<div class='erreur-msg'>Veuillez entrer votre prénom.</div>");
				$('#prenom').addClass('erreur');
				return false;
			}
			

			if ( $("#nom").val().length < 1  ) {
				erreur = 1;
				$('#msg').html("<div class='erreur-msg'>Veuillez entrer votre nom.</div>");
				$('#nom').addClass('erreur');
				return false;
			}
			
			
			if ( $("#courriel").val().length < 1 ) {
				erreur = 1;
				$('#msg').html("<div class='erreur-msg'>Veuillez entrer votre courriel.</div>");
				$('#courriel').addClass('erreur');
				return false;
			}
			
					
			var email = $("#courriel").val();
			var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		
			if (!filter.test(email)) {
				$('#msg').html("<div class='erreur-msg'>Veuillez inscrire un courriel valide.</div>");
				$('#courriel').addClass('erreur');
				return false;
			 }


			if ( $("#telephone").val().length < 1 ) {
				erreur = 1;
				$('#msg').html("<div class='erreur-msg'>Veuillez entrer votre numéro de téléphone.</div>");
				$('#telephone').addClass('erreur');
				return false;
			}
			
			if ( $("#message").val().length < 1 ) {
				erreur = 1;
				$('#msg').html("<div class='erreur-msg'>Veuillez inscrire votre message.</div>");
				$('#message').addClass('erreur');
				return false;
			}
	
	


				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "ajax-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						if(data == 1){
							$('#msg').html("<div class='success-msg'><strong>Merci!</strong><br />Nous avons bien reçu votre message.<br />Nous vous contacterons dans les plus brefs délais.<br /></div></div>");
							
							$('#form-contact').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='erreur-msg'>Une erreur s'est produite, veuillez réessayer plus tard.<br /> Merci!</div>");
							$('#form-contact').hide();
						}
						
						else if(data == 3){
							$('#msg').html("<div class='erreur-msg'>Veuillez remplir tous les champs.</div>");
						}
						
						else if(data == 4){
							$('#msg').html("<div class='erreur-msg'>Veuillez inscrire un courriel valide.</div>");
							$('#courriel').addClass('erreur');
						}
						
						
						
							
							
						return false;
					}
				});
			
				return false;
	});
	

  
  
  
	
});