<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <title>Contactez nos experts en construction | Logisko</title>
    <meta name="description" content="Parlez-nous de vos projets de rénovation et construction résidentielle, industrielle ou commerciale et faites confiance à la réputation d'excellence de Logisko." />
    <link href="dist/css/reset.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="dist/css/sliderpro/slider-pro.min.css" media="screen"/>
    <link href="dist/css/styles.min.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body class="contact">


<?php include('inc/header.inc.php'); ?>


<section id="img-contact"></section>



<section id="contenu-contact">
	<div class="inner">
    
            <h1>Contactez-nous</h1>
            
            <h2 class="sous-titre">Réalisons vos projets ensemble</h2>
            
            <a href="tel:514-863-0817">514 863-0817</a>
            
            
            
            <div class="boite-formulaire">
            
            	<div id="msg"></div>
            
            	<form id="form-contact" name="form-contact" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                
                
                    	<input type="text" id="prenom" name="prenom" value="" placeholder=" Prénom*:" />
                        <input type="text" id="nom" name="nom" value="" placeholder=" Nom*:" />               
                     
                        <input type="text" id="courriel" name="courriel" value="" placeholder=" Courriel*:" />
                 		<input type="text" id="telephone" name="telephone" value="" placeholder=" Téléphone*:" />
                                        
                    
                    <br />
                    <textarea id="message" name="message"  placeholder=" Message*:"></textarea>
    
                        <input type="text" id="subject" name="subject" value="" class="fantome" />
                        <input type="submit" id="btnSubmit" name="btnSubmit" value="ENVOYER" />
                        
                        
				</form>
            
           	</div>
            <!-- boite-formulaire -->

    </div>
    <!-- inner -->
</section>
<!-- contenu -->




<?php include('inc/footer.inc.php'); ?>

</body>
</html>
