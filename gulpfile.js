// require gulp
var gulp = require('gulp');

// require other packages
var concat = require('gulp-concat');
var cssmin = require('gulp-minify-css');
var rename = require("gulp-rename");
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var source = "dist/css";
var sourcejs = "dist/js";



/*

gulp.task('sass', function(){
  return gulp.src('sass/style.scss')
    .pipe(sass()) // Using gulp-sass
    .pipe(gulp.dest('css'));
});
*/

//gulp.watch('sass/**/*.scss', ['sass']); 


/*
gulp.task('default', ['scripts', 'styles', 'watch']);

*/
// scripts task
gulp.task('scripts', function()
{
	return gulp.src('./app/js/*.js')
	   // .pipe(concat('app.js'))
	    .pipe(gulp.dest(sourcejs))
	    .pipe(uglify())
	    .pipe(rename({suffix: '.min'}))
	    .pipe(gulp.dest(sourcejs));
});


// styles task
gulp.task('styles', function() {
  return gulp.src('./app/sass/*.scss')
  	.pipe(plumber())
  	.pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(source))
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(source));
});


gulp.task('watch', function()
{
	gulp.watch('./app/js/*.js', ['scripts']);
	gulp.watch('./app/sass/*.scss', ['styles']);
});