<footer id="footer">
    <div class="inner">
    	
        <h2> Vous aimeriez en savoir plus 
        au sujet de nos services et réalisations? </h2>

        <a href="contactez-nous.php" class="btn hvr-pop">Contactez-nous</a>
        
        
        <div class="copyright">
        	Logisko Immobilier &copy; 2017&nbsp;&nbsp;|&nbsp;&nbsp;Tous droits réservés.
        </div> 
                    
    </div>
</footer>
 <!-- footer --> 
 
    

    <script type="text/javascript" src="dist/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="dist/js/jquery.waypoints.min.js"></script>
    <script type="text/javascript" src="/dist/js/sliderpro/jquery.sliderPro.min.js"></script>
    <script type="text/javascript" src="dist/js/scripts.min.js"></script>


</body>
</html>