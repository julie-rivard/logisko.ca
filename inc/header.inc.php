<section id="header">
	<div class="inner">
    
    	<div id="logo"><a href="http://logisko.ca"></a></div>
        
        <nav>
        
            <a href="#" id="menu-icon">&#9776;</a>

        	<ul id="menu">
            	<li><a href="a-propos.php" <?php if( $_SERVER['REQUEST_URI'] == '/a-propos.php' ){ echo 'class="active"'; } ?>>À propos</a></li>
                <li class="hide-mobile">|</li>
            	<li><a href="prix-et-nominations.php" <?php if( $_SERVER['REQUEST_URI'] == '/prix-et-nominations.php' ){ echo 'class="active"'; } ?>>Prix et nominations</a></li>
                <li class="hide-mobile">|</li>
            	<li><a href="realisations.php" <?php if( $_SERVER['REQUEST_URI'] == '/realisations.php' ){ echo 'class="active"'; } ?>>Réalisations</a></li>
                <li class="hide-mobile">|</li>
            	<li><a href="contactez-nous.php" <?php if( $_SERVER['REQUEST_URI'] == '/contactez-nous.php' ){ echo 'class="active"'; } ?>>Contactez-nous</a></li>
            </ul>
            <!-- menu -->
        </nav>
    
    </div>
    <!-- inner -->
</section>
<!-- header -->