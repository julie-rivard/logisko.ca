<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <title>Construction résidentielle et commerciale | Logisko</title>
    <meta name="description" content="Cumulant plus de 28 ans d’expertise dans les domaines de la construction résidentielle, industrielle et commerciale, nous desservons la grande région métropolitaine." />
    <link href="dist/css/reset.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="dist/css/sliderpro/slider-pro.min.css" media="screen"/>
    <link href="dist/css/styles.min.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>

<?php include('inc/header.inc.php'); ?>




<section id="carrousel">
	<div class="inner">
    
    
    
    <div id="carrousel-accueil" class="slider-pro">
		<div class="sp-slides">
			<div class="sp-slide">
            
                <img class="sp-image" src="dist/css/sliderpro/images/blank.gif" data-src="dist/images/slide-01.jpg" />
			
                <div class="sp-layer sp-blue sp-padding" 
					data-position="topRight"  data-horizontal="0" data-vertical="0" data-width="35%">
					
                    <h3 class="marginplus">28 ans d’expertise</h3>
                        
                    <h4>Notre entreprise se distingue dans la grande région métropolitaine.</h4>
                    
                    <a href="a-propos.php" class="btn">Découvrez notre histoire</a>
                    
                </div>
                
             
                
            </div>
            
            
			<div class="sp-slide">
            
                <img class="sp-image" src="dist/css/sliderpro/images/blank.gif" data-src="dist/images/slide-02.jpg" />
                
                 <div class="sp-layer sp-blue sp-padding" 
					data-position="topRight"  data-horizontal="0" data-vertical="0" data-width="35%">
                    
                    <h3>Construction<br />
						résidentielle</h3>
                        
                    <h4>Condominiums, maisons de ville, maisons unifamiliales, résidences de prestige et plus encore.</h4>
                    
                    <a href="realisations.php" class="btn">Parcourez nos réalisations</a>

                </div>
            </div>
            
            
			<div class="sp-slide">
            
                <img class="sp-image" src="dist/css/sliderpro/images/blank.gif" data-src="dist/images/slide-03.jpg" />
                
                 <div class="sp-layer sp-blue sp-padding" 
					data-position="topRight"  data-horizontal="0" data-vertical="0" data-width="35%">
                    
                    <h3>Accrédité<br />
						Novoclimat 2.0<br />
						et Kaizen</h3>
                        
                    <h4>Nous visons l’excellence en matière d’écoresponsabilité et d’habitation durable.</h4>
                    
                    <a href="contactez-nous.php" class="btn">CONTACTEZ-NOUS</a>

                </div>

            </div>
            
			<div class="sp-slide">
            
                <img class="sp-image" src="dist/css/sliderpro/images/blank.gif" data-src="dist/images/slide-04.jpg" />
                 
                 <div class="sp-layer sp-blue sp-padding" 
					data-position="topRight"  data-horizontal="0" data-vertical="0" data-width="35%">
                                    
                    <h3>Construction industrielle et commerciale</h3>
                        
                    <h4>Des bâtiments de qualité supérieure, pensés et conçus pour répondre aux besoins de votre entreprise.</h4>
                    
                    <a href="realisations.php" class="btn">Parcourez nos réalisations</a>
                </div>

            </div>
            
            
         </div>
    </div>
    
            
    
    </div>
    <!-- inner -->
</section>
<!-- caroussel -->




<section id="info-constructeur">
	<div class="inner">

			<h1>Logisko: votre constructeur d’expérience dans la grande région métropolitaine </h1>
            
            <p>Cumulant plus de 28 ans d’expertise dans les domaines de la construction résidentielle, industrielle et commerciale, notre entreprise est réputée pour la qualité exemplaire de ses réalisations. Récipiendaire de nombreux prix prestigieux, Logisko est une marque de confiance pour tous vos projets de construction, rénovation ou achat de terrains.  
</p>


			 <a href="contactez-nous.php" class="btn hvr-pop">Contactez-nous</a>


    </div>
    <!-- inner -->
</section>
<!-- info-constructeur -->



<section id="expertise">
	<div class="inner">
    
    		<h2>Nos champs d’expertise </h2>
            
            <ul class="liste-expertise">
            	<li>
                	<div class="ico-01"></div>
                	<p>Construction<br />
					résidentielle</p>
                </li>
            	<li>
               		<div class="ico-02"></div>
                	<p>Construction<br />
					industrielle</p>
                </li>
            	<li>
                	<div class="ico-03"></div>
                	<p>Construction<br />
					commerciale</p>
                </li>
            	<li>
                	<div class="ico-04"></div>
                	Construction<br />
					Novoclimat 2.0
                </li>
            	<li>
                	<div class="ico-05"></div>
                	Rénovations
                </li>
            	<li>
                	<div class="ico-06"></div>
                	Achat et vente<br />
					de terrains
                </li>
            </ul>

    
    </div>
    <!-- inner -->
</section>
<!-- expertise -->



<section id="constructeur">
	<div class="inner">

		<h2 class="pop">&laquo; Le plus important constructeur<br />
			de Lanaudière &raquo;</h2>
        <p>- Journal de Montréal</p>

    </div>
    <!-- inner -->
</section>
<!-- constructeur -->



<?php include('inc/footer.inc.php'); ?>


</body>
</html>
