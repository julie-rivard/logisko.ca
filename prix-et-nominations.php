<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <title>Prix et nominations pour projets de construction | Logisko</title>
    <meta name="description" content="Depuis 1989, les réalisations résidentielles et commerciales des entreprises d’Eric Duchaine ont remporté de nombreux prix et nominations prestigieuses." />
    <link href="dist/css/reset.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="dist/css/sliderpro/slider-pro.min.css" media="screen"/>
    <link href="dist/css/styles.min.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>


<?php include('inc/header.inc.php'); ?>


<section id="img-top-prix"></section>



<section id="contenu">
	<div class="inner">
    
            <h1>Prix et nominations </h1>
            
            <p>En 28 ans, les réalisations des entreprises d’Eric Duchaine ont remporté de nombreux prix et nominations prestigieuses. En voici quelques exemples.</p>

<br />
			


			<div class="timeline">
            	<div class="annee-left">1992</div>
            	
                <div class="histoire-right">Jeune Entreprise de l’année Trophée.<br />
					Méritas décerné par les Chambres de commerce de Lanaudière Sud
                </div>
            </div>

			<div class="timeline">
                  <div class="histoire-left">Le plus important constructeur de la région de Lanaudière.<br />
					Désignation par le Journal de Montréal
                  </div>
          		  <div class="annee-right">1996</div>
            	
            </div>


			<div class="timeline">
            	<div class="annee-left">1999</div>
            	
                <div class="histoire-right">Projet multifamilial de l’année.<br />
					Trophée Émérite décerné par Qualité Habitation
            	</div>
            </div>

			<div class="timeline">
                  <div class="histoire-left">Unité d’habitation neuve de 175 000$ et plus.<br />
					Prix Domus décerné par l’A.P.C.H.Q.
                  </div>
          		  <div class="annee-right">2000</div>
            	
            </div>


			<div class="timeline">
            	<div class="annee-left">2000</div>
            	
                <div class="histoire-right">Projet résidentiel de l’année. <br />
					Prix Domus décerné par l’A.P.C.H.Q.
				</div>
            </div>

			<div class="timeline">
                  <div class="histoire-left">Projet au gaz de l’année. <br />
						Nomination
				</div>
          		  <div class="annee-right">2001</div>
            	
            </div>


			<div class="timeline">
            	<div class="annee-left">2005</div>
            	
                <div class="histoire-right">Maison unifamiliale de prestige de l’année. <br />
					Trophée Émérite décerné par Qualité Habitation.
				</div>
            </div>

            
            
    </div>
    <!-- inner -->
</section>
<!-- contenu -->


<?php include('inc/footer.inc.php'); ?>

</body>
</html>
