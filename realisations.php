<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <title>Réalisations résidentielles, industrielles et commerciales | Logisko</title>
    <meta name="description" content="L'entreprise a réalisé de nombreux projets de construction résidentielle, industrielle et commerciale dans les secteurs de Terrebonne, Repentigny et plus encore." />
    <link href="dist/css/reset.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="dist/css/sliderpro/slider-pro.min.css" media="screen"/>
    <link href="dist/css/styles.min.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>


<?php include('inc/header.inc.php'); ?>


<section id="img-realisations"></section>



<section class="contenu">
	<div class="inner">
    
            <h1>Réalisations</h1>
            
            <h2 class="sous-titre">Projets résidentiels</h2>
            
            <p>De nombreux projets de maisons unifamiliales, intergénérations, résidences de prestige et ensembles de condominiums. Environ 400 maisons sur une période de 20 ans, plus particulièrement dans le secteur de Valmont sur Parc, le domaine de la Pinière et le Boisé de la Pinière.</p>

    </div>
    <!-- inner -->
</section>
<!-- contenu -->



<section id="galerie1-realisations">
	<div class="inner">
    
            <ul class="projets">
            
            	
                <li>
                	<img src="dist/images/realisations-03.jpg" width="425" height="277" />
                    <div class="desc">
                    	<h3>9 Place du Bois Clair</h3>
                        <h3>Terrebonne</h3>
                        <h4>2 200 000 $</h4>
                    </div>
                </li>
            	
                        	
                <li>
                	<img src="dist/images/realisations-06.jpg" width="425" height="277" />
                    <div class="desc">
                    	<h3>Maison modèle rue pottier</h3>
                        <h3>Terrebonne</h3>
                        <h4>500 000 $</h4>
                    </div>
                </li>

            	

                <li>
                	<img src="dist/images/realisations-04.jpg" width="425" height="277" />
                    <div class="desc">
                    	<h3>491 à 493, rue Iberville</h3>
                        <h3>Repentigny</h3>
                        <h4>1 000 000 $</h4>
                    </div>
                </li>
                
                        	
                <li>
                	<img src="dist/images/realisations-08.jpg" width="425" height="277" />
                    <div class="desc">
                    	<h3>860, rue Delacroix</h3>
                        <h3>Repentigny</h3>
                        <h4>300 000 $</h4>
                    </div>
                </li>
                
                
            	
                <li>
                	<img src="dist/images/realisations-02.jpg" width="425" height="277" />
                    <div class="desc">
                    	<h3>132 à 136, rue Lapointe</h3>
                        <h3>Repentigny</h3>
                        <h4>2 500 000 $</h4>
                    </div>
                </li>

            
                    
                        	
                <li>
                	<img src="dist/images/realisations-07.jpg" width="425" height="277" />
                    <div class="desc">
                    	<h3>197 à 199, rue Notre-Dame</h3>
                        <h3>Repentigny</h3>
                        <h4>500 000 $ (rénovations)</h4>
                    </div>
                </li>
                    


                        	
                <li>
                	<img src="dist/images/realisations-09.jpg" width="425" height="277" />
                    <div class="desc">
                    	<h3>Domaine de la Pinière</h3>
                        <h3>Terrebonne</h3>
                        <h3>Approximativement 45 maisons de</h3> 
                        <h4>500 000 $</h4>
                    </div>
                </li>

                        	
                <li>
                	<img src="dist/images/realisations-10.jpg" width="425" height="277" />
                    <div class="desc">
                    	<h3>Boisée de la Pinière</h3>
                        <h3>Terrebonne</h3>
                        <h3>Approximativement 20 maisons de</h3> 
                        <h4>850 000 $</h4>
                    </div>
                </li>

            	
                <li>
                	<img src="dist/images/realisations-05.jpg" width="425" height="277" />
                    <div class="desc">
                    	<h3>577, rue Notre-Dame</h3>
                        <h3>Repentigny</h3>
                        <h4>650 000 $ (rénovations)</h4>
                    </div>
                </li>
                    
                
                <li>
                	<img src="dist/images/realisations-01.jpg" width="425" height="277" />
                    <div class="desc">
                    	<h3>988 à 922, rue Notre-Dame</h3>
                        <h3>Repentigny</h3>
                        <h4>9 000 000 $</h4>
                    </div>
                </li>



            </ul>

    </div>
    <!-- inner -->
</section>
<!-- contenu -->




<section class="contenu">
	<div class="inner">
    
            <h2 class="sous-titre">Construction industrielle et commerciale</h2>
            
            <p>Des édifices à caractère industriel et commercial pour petites, moyennes et grandes entreprises.</p>

    </div>
    <!-- inner -->
</section>
<!-- contenu -->



<section id="galerie2-realisations">
	<div class="inner">
    
            <ul class="projets">
                <li>
                	<img src="dist/images/realisations-300-rue-valmont.jpg" width="425" height="277" />
                    <div class="desc">
                    	<h3>300, Valmont</h3>
                        <h3>Repentigny</h3>
                        <h4>1 200 000 $</h4>
                    </div>
                </li>
            
                <li>
                	<img src="dist/images/realisations-950-des-pionniers.jpg" width="425" height="277" />
                    <div class="desc">
                    	<h3>950, rue des Pionniers</h3>
                        <h3>Terrebonne</h3>
                        <h4>13 900 000 $</h4>
                    </div>
                </li>
            
            
            
                <li>
                	<img src="dist/images/realisations-boul-des-entreprises.jpg" width="425" height="277" />
                    <div class="desc">
                    	<h3>2311, boul. des Entreprises, Terrebonne</h3>
                        <h3>Terrebonne</h3>
                        <h4>1 425 000 $</h4>
                    </div>
                </li>
                
            
                <li>
                	<img src="dist/images/realisations-841-champdeau.jpg" width="425" height="277" />
                    <div class="desc">
                    	<h3>8410, rue Champ d’eau</h3>
                        <h3>St-Léonard</h3>
                        <h4>575 000 $ (nouvelle annexe)</h4>
                    </div>
                </li>
            
            
            

            
            </ul>

    </div>
    <!-- inner -->
</section>
<!-- contenu -->




<?php include('inc/footer.inc.php'); ?>

</body>
</html>
